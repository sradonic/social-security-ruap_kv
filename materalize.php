<?php include_once ('checker.php'); ?>
<DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" media="only screen and (color)" href="css/novi.css" />
  <link rel="stylesheet" media="screen and (min-device-width: 992px)" href="css/mobile.css" />
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <script src="plugins/jquery-2.2.3.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script src="js/Chart.bundle.min.js"></script> 
  <script src="js/novi.js"></script>
  <script src="js/sweetalert.min.js"></script> 
  <link rel="stylesheet" type="text/css" href="css/sweetalert.css">
</head>
<body>

<div class="nav-container">
  <nav>
    <div class="nav-wrapper">
      <a href="materalize.php" class="brand-logo center">Social Security</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="left hide-on-med-and-down">
      	 <li><i class="material-icons left">perm_identity</i>Hello, <?php echo $user ?></li>
      </ul>	
      <ul class="right hide-on-med-and-down">
        <li><a href="datatable.php"><i class="material-icons">view_list</i></a></li>
        <li><a href="apikey.php"><i class="material-icons">vpn_key</i></a></li>
        <li><a href="logout.php"><i class="material-icons">input</i></a></li> <!-- zamjeniti logout php sa $user->logout() -->
      </ul>
      <ul class="side-nav" id="mobile-demo">
      	<li><a href="datatable.php">View database</a></li>
      	<li><a href="apikey.php">Use api</a></li>
      	<li><a href="logout.php">Logout</a></li>
      </ul>
    </div>
  </nav>
</div>

<div class="form-container">
	<div class="border-container">
		<div class="input-container">
			<div class="input-field col s12">
	          <input name="family_name" id="family_name" type="text" class="validate">
	          <label for="family_name">Family name</label>
	        </div>
			<div class="input-field col s12">
			    <select name="parents1" id="parents">
			      <option value="" disabled selected>Choose your option</option>
			      <option value="usual">usual</option>
			      <option value="pretentious">pretentious</option>
			      <option value="great_pret">great_pret</option>
			    </select>
			    <label>Parents occupation</label>
		    </div>
		    <div class="input-field col s12">
			    <select name="home1" id="home">
			      <option value="" disabled selected>Choose your option</option>
			      <option value="proper">proper</option>
			      <option value="less_proper">less_proper</option>
			      <option value="improper">improper</option>
			      <option value="critical">critical</option>
			      <option value="very_crit">very_crit</option>
			    </select>
			    <label>Home nursery</label>
		    </div>
		    <div class="input-field col s12">
			    <select name="family1" id="family">
			      <option value="" disabled selected>Choose your option</option>
			      <option value="complete">complete</option>
			      <option value="completed">completed</option>
			      <option value="incomplete">incomplete</option>
			      <option value="foster">foster</option>
			    </select>
			    <label>Family structure</label>
		    </div>
		    <div class="input-field col s12">
			    <select name="housing1" id="housing">
			      <option value="" disabled selected>Choose your option</option>
			      <option value="convenient">convenient</option>
			      <option value="less_conv">less_conv</option>
			      <option value="critical">critical</option>
			    </select>
			    <label>Housing conditions</label>
		    </div>
		    <div class="input-field col s12">
			    <select name="financial1" id="financial">
			      <option value="" disabled selected>Choose your option</option>
			      <option value="convenient">convenient</option>
			      <option value="inconv">inconv</option>
			    </select>
			    <label>Financial standings</label>
		    </div>
		    <div class="input-field col s12">
			    <select name="social1" id="social">
			      <option value="" disabled selected>Choose your option</option>
			      <option value="non_prob">non_prob</option>
			      <option value="slightly_prob">slightly_prob</option>
			      <option value="problematic">problematic</option>
			    </select>
			    <label>Social conditions</label>
		    </div>
		    <div class="input-field col s12">
			    <select name="health1" id="health">
			      <option value="" disabled selected>Choose your option</option>
			      <option value="recommended">recommended</option>
			      <option value="priority">priority</option>
			      <option value="not_recom">not_recom</option>
			    </select>
			    <label>Health conditions</label>
		    </div>
		    <div class="input-field col s12">
		        <input name="children1" id="number" type="text" class="validate">
		        <label for="number">Number od children</label>
	        </div>
	    	<div id="submit-btn">
		    	 <button id="submitBtn" class="btn waves-effect waves-light" onclick="azurerespond()">submit
	    			<i class="material-icons right">send</i>
	  			</button>
	  			<button id="dbBtn" class="btn waves-effect waves-light">Save to db
	    			<i class="material-icons right">send</i>
	  			</button>
  			</div>
		</div>  
	</div>
	<div class="chart-container">
		<canvas id="canvas"></canvas>
	</div>
</div>

<script>
    function azurerespond(){
    	var selectedOption = $("select option:selected");
    if (selectedOption.is(":disabled") || $('#number').val().length === 0 || $('#family_name').val().length === 0) {
    	swal("Nope!", "Some fields are not filled!", "error")
	}
	else {

    $('#dbBtn').css('visibility','visible');
		$('.chart-container').show();
            if ($(window).width() > 992){
                console.log("nesto");
                $('.border-container').css({
                'width' : '35%',
                'margin-left' : '0'
                 });
                $('.chart-container').css({
                    'width': '57%',
                    'margin-right':'1%',
                    'margin-left':'3%',
                    'margin-top': '13%'
                });
            }
            else{
                console.log("nista");
                $('.border-container').css({
                'width' : '100%',
                'margin-left' : '0'
                });
                $('.chart-container').css({
                    'width': '95%',
                    'margin-left': '2.5%',
                    'margin-top': '3%'
                });
            }
        $(window).resize(function(){     
            if ($(window).width() > 992){
                console.log("1");
                $('.border-container').css({
                'width' : '35%',
                'margin-left' : '0%'
                });
                $('.chart-container').css({
                    'width': '57%',
                    'margin-right':'1%',
                    'margin-left':'3%',
                    'margin-top': '13%'
                });
            }
            else{
                console.log("2");
                $('.border-container').css({
                'width' : '100%',
                'margin-left' : '0'
                });
                $('.chart-container').css({
                    'width': '95%',
                    'margin-left': '2.5%',
                    'margin-top':'3%'
                });
              }
       });
    	$.ajax({
    		url: 'ruap.php',
    		type: 'POST',
    		data: {
    				parents1: $('#parents').val(),
    				home1: $('#home').val(),
    				family1: $('#family').val(),
    				housing1: $('#housing').val(),
    				financial1: $('#financial').val(),
    				social1: $('#social').val(),
    				health1: $('#health').val(),
    				children1: $('#number').val(),
    		},
    		success: function(data){
    			var response = JSON.parse(data);
    			var social_not_recom = parseFloat(response.Results.output1.value.Values[0][9])*100;
    			var social_priority = parseFloat(response.Results.output1.value.Values[0][10])*100;
    			var social_spec_prior = parseFloat(response.Results.output1.value.Values[0][11])*100;
    			var social_very_recom = parseFloat(response.Results.output1.value.Values[0][12])*100;
    			var social_scored_labels = response.Results.output1.value.Values[0][13];

    			Chart.defaults.global.defaultFontColor='black';
			    Chart.defaults.global.defaultFontFamily='"Roboto", sans-serif';
				var ctx = document.getElementById("canvas");
				var myChart = new Chart(ctx, {
				    type: 'bar',
				    data: {
				        labels: ["not recom", "priority", "special priority", "very recom"],
				        datasets: [{
				            label: 'Social',
				            data: [social_not_recom, social_priority, social_spec_prior, social_very_recom],
				            backgroundColor: [
				                'rgba(238, 110, 115, 1)',
				                'rgba(238, 110, 115, 1)',
				                'rgba(238, 110, 115, 1)',
				                'rgba(238, 110, 115, 1)'
				            ],
				            borderColor: [
				                'rgba(218, 90, 95, 1)',
				                'rgba(218, 90, 95, 1)',
				                'rgba(218, 90, 95, 1)',
				                'rgba(218, 90, 95, 1)'
				            ],
				            hoverBackgroundColor: [
				                'rgba(238, 110, 115, 0.8)',
				                'rgba(238, 110, 115, 0.7)',
				                'rgba(238, 110, 115, 0.6)',
				                'rgba(238, 110, 115, 0.5)'
				            ],
				            borderWidth: 1
				        }]
				    },
				    options: {
				        scales: {
				            yAxes: [{
				                ticks: {
				                    beginAtZero:true
				                }
				            }]
				        }
				    }
				});
				$('#dbBtn').click(function(){importData(social_not_recom, social_priority, social_spec_prior, social_very_recom, social_scored_labels);});
			     $('#submitBtn').click(function() {
          $('#dbBtn').css('visibility','visible');
        });
      }
		});
    }
}


    function importData(social_not_recom, social_priority, social_spec_prior, social_very_recom, social_scored_labels){
    	var social_not_recom_var = social_not_recom;
    	var social_priority_var = social_priority;
    	var social_spec_prior_var = social_spec_prior;
    	var social_very_recom_var = social_very_recom;
    	var social_scored_labels_var = social_scored_labels;
    	$.ajax({
    		url: 'social.php',
    		type: 'post',
    		data: {
    				user_val: "<?php echo $user ?>",
    				family_name_val: $('#family_name').val(),
    				parents_val: $('#parents').val(),
    				home_val: $('#home').val(),
    				family_val: $('#family').val(),
    				housing_val: $('#housing').val(),
    				financial_val: $('#financial').val(),
    				social_val: $('#social').val(),
    				health_val: $('#health').val(),
    				children_val: $('#number').val(),
    				social_not_recom_val: social_not_recom_var,
    				social_priority_val: social_priority_var,
    				social_spec_prior_val: social_spec_prior_var,
    				social_very_recom_val: social_very_recom_var,
    				social_scored_labels_val: social_scored_labels_var,
    		},
    		success: function(podatak){
    					swal({ 
						  title: "Done!",
						   text: "Values have been inserted!",
						    type: "success" 
						  },
						  function(){
						    window.location.reload(true);
						});
    		}
    	});
	}
</script>

</body>
</html>