<?php
class user {
	public $Username;
	public $Password;

	function __construct($u, $p){
		$this->Username = $u;
		$this->Password = $p;
	}

	public function isLoggedIn() {
		if(isset($_SESSION['user_session']))
		{
			return true;
		}
	}

	public function redirect() {
		header('location: indeks.php');
	}

	public function login($db, $table, $username, $password) {
		try {
			$checkUser = "SELECT Username, Password FROM $table WHERE Username=:Username AND Password=:Password";
			$query = $db->prepare($checkUser);
			$query->bindParam(':Username', $username);
			$query->bindParam(':Password', $password);
			$query->execute();
			$userRow=$query->fetch(PDO::FETCH_ASSOC);
			if($query->rowCount() > 0) {
				$_SESSION['user_session'] = $userRow['Username'];
				return true;
			}
			else {
				return false;
			}
		}
		catch(PDOException $e){
           echo $e->getMessage();
       }
	}		

	public function logout() {
		session_destroy();
		unset($_SESSION['user_session']);
		return true;
	}
}

class Ext_user extends user{
	public $Firstname;
	public $Lastname;
	public $Email;
	public $apikey;


	function __construct($f, $l, $e, $u, $p, $a){
		$this->Firstname = $f;
		$this->Lastname = $l;
		$this->Email = $e;
		$this->Username = $u;
		$this->Password = $p;
		$this->apikey = $a;
	}
}

?>

