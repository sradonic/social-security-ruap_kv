<?php include_once ('checker.php'); 
include_once ('connect.php');
$table='user';
$key=getApikey($db, $table, $user);?>
<DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" media="only screen and (color)" href="css/novi.css" />
  <link rel="stylesheet" media="screen and (min-device-width: 992px)" href="css/mobile.css" />
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <script src="plugins/jquery-2.2.3.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script src="js/Chart.bundle.min.js"></script> 
  <script src="js/novi.js"></script>
  <script src="js/sweetalert.min.js"></script> 
  <link rel="stylesheet" type="text/css" href="css/sweetalert.css">
</head>
<body>

<div class="nav-container">
  <nav>
    <div class="nav-wrapper">
      <a href="materalize.php" class="brand-logo center">Social Security</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="left hide-on-med-and-down">
         <li><i class="material-icons left">perm_identity</i>Hello, <?php echo $user ?></li>
      </ul> 
      <ul class="right hide-on-med-and-down">
        <li><a href="datatable.php"><i class="material-icons">view_list</i></a></li>
        <li><a href="apikey.php"><i class="material-icons">vpn_key</i></a></li>
        <li><a href="logout.php"><i class="material-icons">input</i></a></li> <!-- zamjeniti logout php sa $user->logout() -->
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="materalize.php">Home</a></li>
        <li><a href="datatable.php">View database</a></li>
        <li><a href="logout.php">Logout</a></li>
      </ul>
    </div>
  </nav>
</div>

<div class = "msg-container z-depth-2">
   <h3>Welcome to api page!</h3>
   <p class="flow-text">This page is a guide to using api web service that provides convenient access to data from database. Clients require special actions!</p>
   <br>
   <h4>What can you do?</h4>
   <p class="flow-text">Well, first you need your apiKey: </p>
   <p class="flow-text"><?php echo $key; ?></p>
   <br>
   <ul class="collapsible popout" data-collapsible="accordion">
      <li>
        <div class="collapsible-header"><i class="material-icons">launch</i>Get all values</div>
        <div class="collapsible-body"><p>Get all values by adding this to link and inserting assigned apikey where it says "your api key here": apireq.php?apiKey=YOURAPIKEYHERE</p></div>
     </li>
      <li>
        <div class="collapsible-header"><i class="material-icons">launch</i>Order values</div>
        <div class="collapsible-body"><p>Same like above but with ordering! You choose how to order by...you can choose  between ASC/DSC (simply delete one in ASC/DESC link): apireq.php?apiKey=YOURAPIKEYHERE&&orderby=ASC/DESC</p></div>
     </li>
     <li>
        <div class="collapsible-header"><i class="material-icons">launch</i>Search by index</div>
        <div class="collapsible-body"><p>You can also get a value by its index! you do that by writing the number of the index after "id=": apireq.php?apiKey=YOURAPIKEYHERE&&id=</p></div>
     </li>
   </ul>


</div>