<?php include_once('loginCheck.php'); ?>
<DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" media="only screen and (color)" href="css/login.css" />
  <link rel="stylesheet" media="screen and (min-device-width: 950px)" href="css/login_mobile.css" />
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script src="js/Chart.bundle.min.js"></script> 
  <script src="js/novi.js"></script>
</head>
<body>
<div class="nav-container">
  <nav>
    <div class="nav-wrapper">
      <a href="materialize.php" class="brand-logo center">Social Security</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="mobile.html"><i class="material-icons">perm_identity</i></a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
      	<li><a href="login.php">Login</a></li>
      </ul>
    </div>
  </nav>
</div>

<div class="container">
  <div class="border-container">
    <div class="login-container">
    <form method="POST" id="loginform">
      <div class="input-field col s12">
        <i class="material-icons prefix">account_circle</i>
        <input name ="username" id="icon_prefix" type="text" class="validate">
        <label for="icon_prefix">First Name</label>
      </div>
      <div class="input-field col s12">
        <i class="material-icons prefix">lock</i>
        <input name="password" id="icon_prefix" type="password" class="validate">
        <label for="icon_prefix">Password</label>
      </div>
      <div class="button">
        <button class="btn waves-effect waves-light" type="submit" value="Submit" form="loginform" name="action">Login
         <i class="material-icons right">send</i>
        </button>  
      </div>    
    </form>  
    </div>
  </div>
</div>


</body>
</html>