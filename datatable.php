<?php include_once ('checker.php'); 
$table = 'socialSec';
include_once ('connect.php');
$socials = selectAll($db, $table, $user);
//var_dump($socials);?>
<DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" media="only screen and (color)" href="css/novi.css" />
  <link rel="stylesheet" media="screen and (min-device-width: 992px)" href="css/mobile.css" />
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <script src="plugins/jquery-2.2.3.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script src="js/Chart.bundle.min.js"></script> 
  <script src="js/novi.js"></script>
  <link rel="stylesheet" media="only screen and (color)" href="css/datatableTest.css" />
  <link rel="stylesheet" media="screen and (min-device-width: 992px)" href="css/datatableTestM.css" />  
  <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
  <script type="text/javascript" src="js/jquery.tablesorter.widgets.js"></script>
  <script src="js/sweetalert.min.js"></script> 
  <link rel="stylesheet" type="text/css" href="css/sweetalert.css">
</head>
<body>
<div class="nav-container">
  <nav>
    <div class="nav-wrapper">
      <a href="materalize.php" class="brand-logo center">Social Security</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="left hide-on-med-and-down">
      	 <li><i class="material-icons left">perm_identity</i>Hello, <?php echo $user ?></li>
      </ul>	
      <ul class="right hide-on-med-and-down">
        <li><a href="datatable.php"><i class="material-icons">view_list</i></a></li>
        <li><a href="apikey.php"><i class="material-icons">vpn_key</i></a></li>
        <li><a href="logout.php"><i class="material-icons">input</i></a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
      	<li><a href="materalize.php">Home</a></li>
      	<li><a href="apikey.php">Use api</a></li>
      	<li><a href="logout.php">Logout</a></li>
      </ul>
    </div>
  </nav>
</div>


<div class="table_container">
  <table id="tablica" class="responsive-table highlight centered tablesorter">
    <thead>
      <tr>
      	  <th data-field="social_label">Social label</th>
      	  <th data-field="family_name">Family</th>
          <th data-field="parents">Parents occupation</th>
          <th data-field="home">Home nursery</th>
          <th data-field="family">Family structure</th>
          <th data-field="housing">Housing conditions</th>
          <th data-field="finance">Financial standings</th>
          <th data-field="social">Social conditions</th>
          <th data-field="health">Health conditions</th>
          <th data-field="children">Children number</th>
      </tr>
    </thead>
    <tbody>
		<?php foreach($socials as $social): $social_info = array($social['social_not_recom'],$social['social_priority'],$social['social_spec_prior'],$social['social_very_recom'], $social['social_scored_labels']);?>
		<tr data-info=<?php echo json_encode($social_info);?> class="clickable_row" id=<?php echo $social['id'];?>>
			<td class="colored"><?php echo $social['social_scored_labels'];?></td>
			<td><?php echo $social['family_name'];?></td>
			<td><?php echo $social['parents'];?></td>
			<td><?php echo $social['home'];?></td>
			<td><?php echo $social['family'];?></td>
			<td><?php echo $social['housing'];?></td>
			<td><?php echo $social['finance'];?></td>
			<td><?php echo $social['social'];?></td>
			<td><?php echo $social['health'];?></td>
			<td><?php echo $social['children'];?></td>
		</tr>
		<?php endforeach;?>
    </tbody>
  </table>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('#tablica td.colored').each(function(){
        if ($(this).text() == 'very_recom') {
            $(this).css('background-color','#80cbc4');
        }
        else if($(this).text() == 'spec_prior') {
            $(this).css('background-color','#80deea');
     	}
       	else if($(this).text() == 'priority') {
            $(this).css('background-color','#81d4fa');
      	}
      	else if($(this).text() == 'not_recom') {
            $(this).css('background-color','#ef9a9a');
      	}
  	});
});

$(document).ready(function($) {
  $('.clickable_row').click(function(){
  	var serviceID = this.id;
  	 var tr = $(this).closest('tr');
    var social_inf = JSON.parse($(this).attr('data-info'));
   console.log(tr);

    swal ({
      title: 'Are you sure?',
      text: 'You will not be able to recover this family row!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      closeOnConfirm: false,
      closeOnCancel: false,
  	},
  	function (isConfirm){
  		if(isConfirm){
  			$.ajax({
  				type:"POST",
  				url:"delete.php",
  				data: {id_val:serviceID},
  				success: function(msg) {
  					 tr.fadeOut(1000, function(){
                        $(this).remove();
                    });
  				}
  			});
  			swal("Deleted!", "This family row been deleted.", "success");
  		}
  		else { 
  			swal("Cancelled", "This family row is safe :)", "error");   
  		}
  	});

    });
}); 


$(function(){
  $("#tablica").tablesorter({
  		sortReset   : true,
    	sortRestart : true,
  		cssAsc: 'up',
		cssDesc: 'down',
		cssNone: ''
  });
});
</script>

</body>
</html>