<?php
require_once('connect.php');
class social {
	public $uname;
	public $family_name;
	public $parents;
	public $home;
	public $family;
	public $children;
	public $housing;
	public $finance;
	public $social;
	public $health;
	public $social_not_recom;
	public $social_priority;
	public $social_spec_prior;
	public $social_very_recom;
	public $social_scored_labels;

	function __construct($u, $fn, $pa, $ho, $fa, $ch, $ha, $fi, $so, $he, $nr, $pr, $sp, $vr, $sl){
		$this->uname = $u;
		$this->family_name= $fn;
		$this->parents = $pa;
		$this->home = $ho;
		$this->family = $fa;
		$this->children = $ch;
		$this->housing = $ha;
		$this->finance = $fi;
		$this->social = $so;
		$this->health = $he;
		$this->social_not_recom = $nr;
		$this->social_priority = $pr;
		$this->social_spec_prior = $sp;
		$this->social_very_recom = $vr;
		$this->social_scored_labels = $sl;
	}
}
$Uname=$_POST['user_val'];
$Family_name=$_POST['family_name_val'];
$Parents=$_POST['parents_val'];
$Home=$_POST['home_val'];
$Family=$_POST['family_val'];
$Children=$_POST['children_val'];
$Housing=$_POST['housing_val'];
$Finance=$_POST['financial_val'];
$Social=$_POST['social_val'];
$Health=$_POST['health_val'];
$Social_not_recom=$_POST['social_not_recom_val'];
$Social_priority=$_POST['social_priority_val'];
$Social_spec_prior=$_POST['social_spec_prior_val'];
$Social_very_recom=$_POST['social_very_recom_val'];
$Social_scored_labels=$_POST['social_scored_labels_val'];

$values = new social($Uname, $Family_name, $Parents, $Home, $Family, $Children, $Housing, $Finance, $Social, $Health, $Social_not_recom, $Social_priority, $Social_spec_prior, $Social_very_recom, $Social_scored_labels);
$table = 'socialSec';

$insert = insert($db, $table, $values);
?>