<?php

session_start();

$servername = "localhost";
$username = "root";
$password = "Abyssus";

try {
    $db = new PDO("mysql:host=$servername;dbname=KV_socialSec", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully"; 
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

include_once 'user.php';
$user = new user();

function insert($db, $table, $values) {
	try{
		$array = get_object_vars($values); //get_object_vars - returns array of objects 
		$keys = array_keys($array); //array_keys - returns the keys from the array
		$fields = implode(", ", $keys);
		$value = ":" . implode(", :", $keys);
		$insert = "INSERT INTO $table ($fields) VALUES ($value)";
		$query = $db->prepare($insert);
		//binding values 
		foreach($values as $key => &$value){
			$query->bindParam(':' . $key, $value);
		}
		$query->execute();
	}
	catch(PDOException $e){
		 echo 'ERROR: ' . $e->getMessage();
	}
}

//maybe better use this function only for cheking if there are values (using it for creating tables and lists from db)
function selectParam($db, $table, $values) {
	$array = get_object_vars($values); //get_object_vars - returns array of objects 
	$keys = array_keys($array); //array_keys - returns the keys from the array
	$val = array_values($array);
	$numOfVal = count($keys);
	$fields = implode(", ", $keys);
	$value = ":" . implode(", :", $keys);
	$first_key = array_values($keys)[0]; 
	$first_val = array_values($array)[0]; 
	$first_array = ":" .$first_key;	
	$i=0;
	foreach($values as $key => &$value){
		$selector .= $key . "=" . ":" . $key;
		$i++;
		if($i < $numOfVal){
			$selector .= ' AND ';
		}		
	}	
	$select = "SELECT * FROM $table WHERE $selector";
	$query = $db->prepare($select);
	foreach($values as $key => &$value){
		$query->bindParam(':' . $key, $value);
	}
	$query->execute();
	$userRow=$query->fetchAll(PDO::FETCH_ASSOC);
	return $userRow;
}

function selectAll($db, $table, $uname) {
	$select = "SELECT * FROM $table WHERE uname=:uname";
	$query = $db->prepare($select);
	$query->bindParam(':uname', $uname);
	$query->execute();
	return $query->fetchAll();
}

function delete($db, $table, $id){
	$delete = "DELETE FROM $table WHERE id=:id";
	$query = $db->prepare($delete);
	$query->bindParam(':id', $id);
	$query->execute();
}

function getApikey($db, $table, $user){
	$selector = "SELECT apikey FROM $table WHERE Username=:Username";
	$query = $db->prepare($selector);
	$query->bindParam(':Username', $user);
	$query->execute();
	$apirow=$query->fetchColumn();
	if(count($apirow) == 1){
		return $apirow;
	}
	return false;
}

function checkApikey($db, $table, $apikey){
	$selector = "SELECT Username FROM $table WHERE apikey=:apikey";
	$query = $db->prepare($selector);
	$query->bindParam(':apikey', $apikey);
	$query->execute();
	$userRows=$query->fetchColumn();
	if(count($userRows) == 1){
		return $userRows;
	}
	return false;
}

function apiSelect($db, $tableApi, $uname){
	$selector = "SELECT * FROM $tableApi WHERE uname=:uname";
	$query = $db->prepare($selector);
	$query->bindParam(':uname', $uname);
	$query->execute();
	$apiRows=$query->fetchAll();
		return $apiRows;	
}

function apiSelectOrder($db, $tableApi, $uname, $order, $limit){
	$selector = "SELECT * FROM $tableApi WHERE uname=:uname ORDER BY id $order LIMIT $limit";
	$query = $db->prepare($selector);
	$query->bindParam(':uname', $uname);
	$query->execute();
	$apiRows=$query->fetchAll();
		return $apiRows;	
}

function apiSelectId($db, $tableApi, $uname, $id){
	$selector = "SELECT * FROM $tableApi WHERE uname=:uname AND id=:id";
	$query = $db->prepare($selector);
	$query->bindParam(':uname', $uname);
	$query->bindParam(':id', $id);
	$query->execute();
	$apiRows=$query->fetchAll();
		return $apiRows;	
}

?>